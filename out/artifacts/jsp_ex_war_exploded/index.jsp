<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="ro.gr8.data.Item" %>
<%@ page import="ro.gr8.data.ItemBuilder" %>
<%@ page import="ro.gr8.repository.Repository" %><%--
  Created by IntelliJ IDEA.
  User: B
  Date: 10/14/2019
  Time: 8:24 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>My JSP</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.deep_purple-blue.min.css"/>
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/script.js"></script>

</head>
<body>
<%
    request.setAttribute("items", Repository.getRepo().getItemList());
%>
<div id="main_container">
    <div id="input_div">
        <form action="${pageContext.request.contextPath}/toDoItem" method="POST" id="form_id">
            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label " id="text_input_div"
                 style="width: 100%">
                <input class="mdl-textfield__input" type="text" id="sample3" name="input_item_name">
                <label class="mdl-textfield__label" for="sample3" id="label_id">Add Item</label>
            </div>
        </form>
    </div>
    <div id="table_div">
        <ul class="demo-list-control mdl-list">
        <%
            StringBuilder isSelected = new StringBuilder(" ");
            StringBuilder textDecoration = new StringBuilder("");
            List<Item> items = Repository.getRepo().getItemList();
            for (int i = 0; i < items.size(); i++) {
                Item item = items.get(i);
                textDecoration.delete(0, textDecoration.length());
                isSelected.delete(0, isSelected.length());
                if (item.isChecked()) {
                    isSelected.append("checked");
                    textDecoration.append("line-through");
                } else {
                    textDecoration.append("none");
                }
        %>


            <li class="mdl-list__item">
    <span class="mdl-list__item-primary-content" style="text-decoration: <%=textDecoration%>" id="span-id-<%=item.getName()%>">
     <%=item.getName()%>
    </span>
    <form action="${pageContext.request.contextPath}/checkbox" method="post" id="form-id-<%=i%>" name="check">
        <span class="mdl-list__item-secondary-action">
            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="<%=item.getName()%>">
                <input type="checkbox" id="<%=item.getName()%>" class="mdl-checkbox__input checkboxes"
                    name="checkbox-input" <%=isSelected%> />
            </label>
        </span>
    </form>
            </li>

        <%
            }
        %>
        </ul>
    </div>


    <div id="button_div_id">
        <form action="${pageContext.request.contextPath}/delete" method="post" id="button_delete_form">
            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--primary"
                    id="dustbin_id">
                <img src="https://img.icons8.com/metro/26/000000/delete.png" style="filter: invert(95%) ">
            </button>
        </form>

    </div>


</div>

</body>
</html>
