$(document).ready(function () {

// POST the checkbox name and the value
    $(".checkboxes").on("change", function (e) {
        e.preventDefault();
        var checkboxId = $(this).attr('id');
        var checkboxValue = $("#" + checkboxId).is(":checked");
        var spanSelector = $('#span-id-' + checkboxId);
        if (checkboxValue === true) {
            spanSelector.css('text-decoration', 'line-through');
            spanSelector.css('color', 'gray');
        } else {
            $('#span-id-' + checkboxId).css('text-decoration', 'none');
            spanSelector.css('color', 'black');
        }
        $.ajax({
            url: window.location.pathname + 'checkbox' + '?checkboxValue=' +
                checkboxValue + '&checkboxId=' + checkboxId,
            cache: false,
            contentType: 'text/html;charset=UTF-8',
            type: 'POST',
            success: function (result) {

            },
            error: function (xhr, ajaxOption, thrownError) {
                console.log(thrownError)
            }
        });

        $.get(window.location.pathname + 'checkbox', function (data) {
            if (data.trim() === "true") {
                $("#dustbin_id").css("background-color", "rgb(103, 58, 183)");
            } else if (data.trim() === "false") {
                $("#dustbin_id").css("background-color", "gray");
            }
        })
    });

    // delete
    $("#dustbin_id").one('click', function (e) {
        var deleteKey = 'delete';
        $.ajax({
            url: window.location.href + 'delete' + '?value=' + deleteKey,
            cache: false,
            contentType: 'text/html;charset=UTF-8',
            type: 'post',
            success: function (result) {
                console.log(result)
            },
            error: function (xhr, ajaxOption, thrownError) {
                console.log(thrownError)
            }
        });
        e.stopImmediatePropagation();
    })
});
