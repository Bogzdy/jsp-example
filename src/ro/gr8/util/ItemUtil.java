package ro.gr8.util;

import ro.gr8.data.Item;

import java.util.List;

public class ItemUtil {
    public static boolean isNotNull(String str) {
        return (str != null);
    }

    public static boolean isNotNull(Item item) {
        return (item != null);
    }

    private static boolean isNotEmpty(String str) {
        return !str.trim().isEmpty();
    }

    public static boolean isAValidItem(Item item, List<Item> itemList) {
        return ItemUtil.isNotNull(item) && !itemList.contains(item) && ItemUtil.isNotNull(item.getName()) && ItemUtil.isNotEmpty(item.getName());
    }
}
