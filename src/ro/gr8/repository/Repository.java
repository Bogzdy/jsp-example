package ro.gr8.repository;

import ro.gr8.data.Item;
import ro.gr8.util.ItemUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class Repository {
    private static Repository repo = new Repository();
    private List<Item> itemList = new ArrayList<>();

    private Repository() {
    }

    public static Repository getRepo() {
        return repo;
    }

    public List<Item> getItemList() {
        return Collections.unmodifiableList(itemList);
    }

    public void addItem(Item item) {
        if (ItemUtil.isAValidItem(item, itemList)) {
            itemList.add(0, item);
        }
    }

    public void removeCheckedItems() {
        if (!itemList.isEmpty()){
            itemList.removeIf(item -> (item.isChecked()));
        }
    }

    public void changeState(Item item, boolean state) {
        if (ItemUtil.isNotNull(item)){
            int itemIndex = itemList.indexOf(item);
            itemList.get(itemIndex).setChecked(state);
        }
    }

    public boolean hasCheckedItems() {
        boolean bool = false;
        for (Item item : itemList) {
            if (item.isChecked()) {
                bool = true;
                return bool;
            }
        }
        return bool;
    }

    public Item findItemByName(String name) {
        Item returnedItem = null;
        for (Item item : itemList) {
            if (ItemUtil.isNotNull(name) && item.getName().equals(name)) {
                returnedItem = item;
                break;
            }
        }
        return returnedItem;
    }
}
