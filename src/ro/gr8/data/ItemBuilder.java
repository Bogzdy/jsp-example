package ro.gr8.data;

public final class ItemBuilder {
    private String name;
    private boolean isChecked;
    private int id;

    private ItemBuilder() {
    }

    public static ItemBuilder anItem() {
        return new ItemBuilder();
    }

    public ItemBuilder id(int id) {
        this.id = id;
        return this;
    }

    public ItemBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder isChecked(boolean isChecked) {
        this.isChecked = isChecked;
        return this;
    }

    public Item build() {
        return new Item(this.id, this.name, this.isChecked);
    }
}
