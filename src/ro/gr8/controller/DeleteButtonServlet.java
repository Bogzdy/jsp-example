package ro.gr8.controller;

import ro.gr8.repository.Repository;
import ro.gr8.util.ItemUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class DeleteButtonServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("deleteButtonServlet called!");
        String value = req.getParameter("value");
        if (value != null && value.trim().equals("delete")) {
            Repository.getRepo().removeCheckedItems();
        }
        resp.sendRedirect(getServletContext().getContextPath());
    }

}
