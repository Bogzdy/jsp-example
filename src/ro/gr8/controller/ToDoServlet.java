package ro.gr8.controller;

import ro.gr8.data.Item;
import ro.gr8.data.ItemBuilder;
import ro.gr8.repository.Repository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ToDoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        out.println(req.getAttribute("input_item_name"));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
        String submittedItem = request.getParameter("input_item_name");
        Item itm = ItemBuilder.anItem()
                .name(submittedItem)
                .isChecked(false)
                .build();
        Repository.getRepo().addItem(itm);
        System.out.println("Item " + itm.getName() + " was created");

        resp.sendRedirect(getServletContext().getContextPath());
    }

//    @Override
//    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String actionName = req.getParameter("input_item_name");
//        Item item = Repository.getRepo().findItemByName(actionName);
//
//        if (item == null){
//            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
//            return;
//        }
//        item.setChecked(true);
//        resp.sendRedirect(getServletContext().getContextPath());
//    }
}
