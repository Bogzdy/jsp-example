package ro.gr8.controller;

import ro.gr8.data.Item;
import ro.gr8.repository.Repository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CheckboxServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String checkboxValue = req.getParameter("checkboxValue");
        String checkboxId = req.getParameter("checkboxId");
        System.out.println(checkboxValue);
        boolean checkbValue = false;
        if (checkboxValue.equals("true")) {
            checkbValue = true;
        } else {
            checkbValue = false;

        }
        Repository.getRepo().changeState(Repository.getRepo().findItemByName(checkboxId), checkbValue);
        resp.sendRedirect(getServletContext().getContextPath());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.getWriter().println(Repository.getRepo().hasCheckedItems());
    }
}
